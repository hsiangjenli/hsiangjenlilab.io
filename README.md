```python
conda create --name flaskf python=3.8
```
```python
conda activate flaskf
```
```python
pip install -r requirements.txt
```
```python
pip freeze > requirements-block.txt
```

set FLASK_APP=website
set FLASK_ENV=development

set FLASK_APP=website flask freeze