---

Title: 😶 About me  
Date: 2022-02-07

---

## **李享紝**

🎓 **國立高雄科技大學 金融系（2022）**  
✉️ **hsiangjenli@gmail.com**  
🔧 **Python, Flask, HTML, CSS, MySQL**  


## **證照**

1. ***Microsoft-Azure AI Fundamentals***
2. **金融市場常識與職業道德**
3. **信託業業務人員**
4. **證券商業務員**
5. **金融科技力**

## **經歷**

- **2021.09 ~ 2022.01-國立高雄科技大學-高瞻科技不分系-資料科學 助教**

## **競賽經驗**

1. **2022-國立高雄科技大學-電機系-人工智慧暨類神經網路聯合專題展 🥇 第一名**
2. **2020-Fintech Space-校園成果展 🏅 優選**

---

1. **2021-國立台北商業大學-Fintech金融服務-零股交易📈**
5. **2020-國立台北商業大學-Fintech金融服務-記帳軟體💸**
6. **2020-兆豐銀行-Fintech創意競賽**
7. **2020-⽟⼭銀⾏-商業競賽**
8. **2020-國立高雄科技大學-全國大專院校智慧應用創新創意競賽**

## **Project**

1. **2022-比特幣價格/漲跌預測**
1. **TWSE｜台灣證券交易所爬蟲**
1. **ANUE｜鉅亨網爬蟲**

---

1. **📱2020-LINE BOT**
2. **🎲2022-Teachable Machine --Tic-Tac-Toe**

## 修課經歷

1. **高科-財管碩-量化投資與程式交易**
2. **高科-電機系-人工智慧**
3. **高科-工管系-資料探勘導論**
4. **高科-博雅教育中心-程式設計教學助教培訓-推動大學{程式設計}教學**

---

1. ***Coursera-IBM-Exploratory Data Analysis for Machine Learning***
2. ***Coursera-IBM-Supervised Machine Learning - Regression***
3. ***AWS-Math for Machine Learning***
4. **_NVIDIA_-深度學習基礎理論與實踐**