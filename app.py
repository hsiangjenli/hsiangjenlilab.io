from flask import Flask, render_template, url_for
from flask_flatpages import FlatPages
from flask_frozen import Freezer
import markdown2

DEBUG = True
FLATPAGES_AUTO_RELOAD = DEBUG
FLATPAGES_EXTENSION = '.md'

app = Flask(__name__)
app.config.from_object(__name__)
app.config['FREEZER_BASE_URL'] = 'https://hsiangjenli.gitlab.io/'
app.config['FREEZER_DESTINATION'] = 'public'

pages = FlatPages(app)
freezer = Freezer(app)

extras = ["fenced-code-blocks"]

@app.route('/')
def homepage():
    return render_template('home/index.html')

@app.route('/blog-posts/')
def blogposts():
    return render_template('blog/index.html', pages=pages)

@app.route('/about-me/')
def aboutme():
    html = markdown2.markdown_path(f'pages/About-me.md', extras=extras)
    return render_template('blog/page.html', html=html)

@app.route('/<path:path>.html')
def page(path):
    page = pages.get_or_404(path)
    html = markdown2.markdown_path(f'pages/{path}.md', extras=extras)
    return render_template('blog/page.html', page=page, html=html)

@freezer.register_generator
def pagelist():
    print(pages)
    for page in pages:
        yield url_for('page', path=page.path)

if __name__ == '__main__':
    
    freezer.freeze()
    
    # app.run(host='0.0.0.0', port=5001)



